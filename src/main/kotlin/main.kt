import com.github.kwhat.jnativehook.GlobalScreen
import com.github.kwhat.jnativehook.NativeHookException
import kotlin.system.exitProcess


fun main() {

    if (!GlobalScreen.isNativeHookRegistered()) {
        try {
            GlobalScreen.registerNativeHook()
        } catch (e: NativeHookException) {
            e.printStackTrace()
            exitProcess(-1)
        }
    }
    GlobalScreen.addNativeKeyListener(TaskEndsPlayer("./voice/end_of_task/"))

    //定期再生を何分ごとにするか
    val period_minute = 10

    val periodicPlayer = RandomPlayer("./voice/periodically/")

    while (true){
        periodicPlayer.exec()
        Thread.sleep(period_minute * 60000L)
    }

}