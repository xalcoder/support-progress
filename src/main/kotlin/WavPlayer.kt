import java.io.File
import java.io.IOException
import java.net.MalformedURLException
import javax.sound.sampled.*

object WavPlayer {

    private var clip: Clip? = null

    fun playNew(wavFilePath: String){
        clip?.stop()

        val wavFile = File(wavFilePath)

        try {
            AudioSystem.getAudioInputStream(wavFile).use { audioInputStream ->

                //ファイルの形式取得
                val audioFormat = audioInputStream.format

                //単一のオーディオ形式を含む指定した情報からデータラインの情報オブジェクトを構築
                val dataLine = DataLine.Info(Clip::class.java, audioFormat)

                //指定された Line.Info オブジェクトの記述に一致するラインを取得
                clip = AudioSystem.getLine(dataLine) as Clip

                //再生
                clip?.open(audioInputStream)
                clip?.start()
            }
        } catch (e: MalformedURLException) {
            e.printStackTrace()
        } catch (e: UnsupportedAudioFileException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: LineUnavailableException) {
            e.printStackTrace()
        }
    }
}