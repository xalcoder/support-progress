import com.github.kwhat.jnativehook.keyboard.NativeKeyEvent
import com.github.kwhat.jnativehook.keyboard.NativeKeyListener


class TaskEndsPlayer(directoryPath: String): NativeKeyListener{

    var isCtrlPressed = false

    private val CTRL = 29
    private val S = 31

    private val randomPlayer = RandomPlayer(directoryPath)

    // キーが押された
    override fun nativeKeyPressed(e: NativeKeyEvent) {
        if(e.keyCode == CTRL){
            isCtrlPressed = true
        }

        if(isCtrlPressed && e.keyCode == S){
            randomPlayer.exec()
        }
    }

    // キーが離された
    override fun nativeKeyReleased(e: NativeKeyEvent) {
        if(e.keyCode == CTRL){
            isCtrlPressed = false
        }
    }

}