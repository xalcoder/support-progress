import java.io.File

class RandomPlayer(private val directoryPath: String) {

    private val wavFileNames: List<String>
        = File(directoryPath).walk()
            .filter { file -> file.extension == "wav" } //kotlinは==で文字列比較可能
            .map { file -> file.name }
            .toList()

    fun exec(){
        val fileName = wavFileNames.random()
        println("playing: $fileName")
        WavPlayer.playNew(directoryPath + fileName)
    }
}